abstract class Acara {
    String judul;
    String genre;
    int durasi;

    public Acara(String judul, String genre, int durasi) {
        this.judul = judul;
        this.genre = genre;
        this.durasi = durasi;
    }

    public String getJudul() {
        return judul;
    }

    public String getGenre() {
        return genre;
    }

    public int getDurasi() {
        return durasi;
    }
    
    abstract void play();
}

class Movie extends Acara{
    public Movie(String judul, String genre, int durasi){
        super(judul, genre, durasi);
    }
    
    public void play(){
        System.out.println("Memutar Film "+ judul);
    }
}

class Series extends Acara{
    int episode;
    public Series (String judul, String genre, int durasi, int episode){
        super(judul, genre, durasi);
        this.episode = episode;
    }
    
    public void play(){
        System.out.println("Memutar Series "+ judul + " Episode "+ episode);
    }
}

public class Main {
    public static void main(String[] args) {
        Acara movie = new Movie("Joker", "Thriller", 122);
        Acara series = new Series("Stranger Things", "Sci-Fi", 50, 1);

        movie.play();
        series.play();
    }
}
